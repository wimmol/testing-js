import CartParser from './CartParser';
import cartJson from '../samples/cart'

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	describe('parseLine', () => {
		it('should return correct object fields', () => {
			const currentResult = parser.parseLine('Consectetur adipiscing,28.72,10');
			const expectedResult = {
				id: 'id',
				name: "Consectetur adipiscing",
				price: 28.72,
				quantity: 10
			};

			expect(currentResult.name).toEqual(expectedResult.name);
			expect(currentResult.price).toEqual(expectedResult.price);
			expect(currentResult.quantity).toEqual(expectedResult.quantity);
		});
		it('should return expected object keys', () => {
			const currentResult = parser.parseLine('Consectetur adipiscing,28.72,10');
			const expectedRsult = {
				id: 'id',
				name: "Consectetur adipiscing",
				price: 28.72,
				quantity: 10
			};

			expect(Object.keys(expectedRsult).length)
				.toBe(Object.keys(currentResult).length);

			Object.keys(expectedRsult).forEach(key =>{
				expect(Object.keys(currentResult)).toContain(key);
			})
		});
	});

	describe('validate', () => {
		it('should return header error, when header fields dont match with expected ones', () => {
			const testCsv = [
				'Product name,Price\n' +
				'Mollis consequat,9.00,2',
				'Product name,Quantity\n' +
				'Mollis consequat,2',
				'Price,Product name,Quantity\n' +
				'Mollis consequat,9.00,2'
			];
			testCsv.forEach(test => {
				expect(parser.validate(test).map(item => item.type)).toContain('header');
			});
		});
		it('should return row error, when some body fields are missing', () => {
			const testCsv = [
				'Product name,Price,Quantity\n' +
				'Mollis consequat,2',
				'Product name,Quantity\n' +
				'Mollis consequat,2',
				'Product name\n' +
				'Mollis consequat'
			];
			testCsv.forEach(test => {
				expect(parser.validate(test).map(item => item.type)).toContain('row');
			});
		});
		it('should return cell error, when string fields is empty', () => {
			const testCsv = 'Product name,Price,Quantity\n' +
				',9.00,2';
			expect(parser.validate(testCsv)[0]).toEqual({
				column: 0,
				message: 'Expected cell to be a nonempty string but received "".',
				row: 1,
				type: "cell"
			})
		});
		it('should return cell error, when number field is not positive number', () => {
			const testVal = [-9.00, 'aaaaaa', true, {i: 'i'}, null, undefined];
			testVal.forEach(testVal => {
				let testCsv = 'Product name,Price,Quantity\n' +
					`Mollis consequat,${testVal},2`;
				expect(parser.validate(testCsv)[0]).toEqual({
					column: 1,
					message: `Expected cell to be a positive number but received "${testVal}".`,
					row: 1,
					type: "cell"
				});
			});
		});
		it('should return no errors', () => {
			const testCsv = 'Product name,Price,Quantity\n' +
				'Mollis consequat,9.00,2\n' +
				'Tvoluptatem,10.32,1\n' +
				'Scelerisque lacinia,18.90,1\n' +
				'Consectetur adipiscing,28.72,10\n' +
				'Condimentum aliquet,13.90,1\n';
			expect(parser.validate(testCsv)).toEqual([]);
		})
	});
	describe('parse', () => {
		it('should throw error, when contents is not valid', () => {
			parser.readFile = jest.fn();
			parser.readFile.mockReturnValueOnce('bla,bla,bla');
			expect(parser.parse.bind(parser)).toThrow('Validation failed!');
		});
		it('should return parsed data and calculated total', () => {
			parser.readFile = jest.fn();
			parser.readFile.mockReturnValueOnce('Product name,Price,Quantity\n' +
				'Mollis consequat,9.00,2\n');
			const expectedResult = {
				"items": [{
						"id": "id",
						"name": "Mollis consequat",
						"price": 9,
						"quantity": 2
					}],
				"total": 18};
			const parsedData = parser.parse('any path');
			parsedData.items[0].id = 'id';
			expect(parsedData.items).toEqual(expectedResult.items);
			expect(parsedData.total).toBe(expectedResult.total);
		});
	});
});

describe('CartParser - integration test', () => {
	it('should parse data', () => {
		const path = __dirname.slice(0, -3) + 'samples/cart.csv';
		const parsedData = parser.parse(path);
		const expectedResult = cartJson.items.map(item => ({...item, id: 'id'}));
		const currentResult = parsedData.items.map(item => ({...item, id: 'id'}));
		expect(currentResult).toEqual(expectedResult);
		expect(parsedData.total).toBe(cartJson.total);
	});
});